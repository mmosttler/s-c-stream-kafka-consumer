package com.example.beans;

/**
 * 
 */
public class SampleRequest
{
    private String key;
    private SampleContent content;
    
    /**
     * Constructor
     */
    public SampleRequest() {
        // Empty
    }

    /**
     * @return the key
     */
    public String getKey()
    {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key)
    {
        this.key = key;
    }

    /**
     * @return the content
     */
    public SampleContent getContent()
    {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(SampleContent content)
    {
        this.content = content;
    }

}
