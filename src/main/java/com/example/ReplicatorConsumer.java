package com.example;

import java.util.function.Consumer;

import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.beans.SampleRequest;

/**
 * 
 */
@SpringBootApplication
public class ReplicatorConsumer
{
    private static final Logger logger = LoggerFactory.getLogger(ReplicatorConsumer.class);
    
    public static void main(String[] args)
    {
        SpringApplication.run(ReplicatorConsumer.class, args);
    }

    @Bean
    public Consumer<KStream<Object, SampleRequest>> consume()
    {
        return kstream -> kstream.peek((key, value) -> checkIt(value));
    }
    
    private SampleRequest checkIt(SampleRequest sampleRequest)
    {
        logger.info("Key: {}", sampleRequest.getKey());
        return sampleRequest;
    }
}
